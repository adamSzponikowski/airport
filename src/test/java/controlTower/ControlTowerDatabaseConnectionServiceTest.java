package controlTower;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import planes.Plane;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class ControlTowerDatabaseConnectionServiceTest {

    private ControlTowerDatabaseConnectionService controlTowerDatabaseConnectionService = new ControlTowerDatabaseConnectionService();
    private IDGenerator idGenerator = new IDGenerator();
    private DatabaseConnection databaseConnection = DatabaseConnection.getInstance();
    private Connection connection = databaseConnection.getConnection();
    Plane plane = new Plane();

    @Test
    public void isaPlaneBeingAddedToTheDataBase() throws SQLException {
        //Given
        idGenerator.generateUniqueRandomID(plane.planeInfo);
        //When
        controlTowerDatabaseConnectionService.insertAPlaneIntoDatabase(plane.planeInfo);
        //Then
        String query = "SELECT * FROM planes";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        ResultSet resultSet = preparedStatement.executeQuery();
        int rowCount = 0;
        while (resultSet.next()) {
            rowCount++;
        }

        String queryFlight = "SELECT * FROM flight_data";
        PreparedStatement preparedStatementFlight = connection.prepareStatement(queryFlight);
        ResultSet resultSetFlight = preparedStatementFlight.executeQuery();
        int rowCountFlight = 0;
        while (resultSetFlight.next()) {
            rowCountFlight++;
        }

        assertEquals(1, rowCount);
        assertEquals(1, rowCountFlight);
    }

    @AfterEach
    public void afterEach() throws SQLException {
        String deletePlaneQuery = "DELETE FROM planes WHERE plane_id = ?";
        String deleteFlightDataQuery = "DELETE FROM flight_data WHERE plane_id = ?";

        PreparedStatement deletePlaneStatement = connection.prepareStatement(deletePlaneQuery);
        deletePlaneStatement.setInt(1, plane.planeInfo.getId());
        deletePlaneStatement.executeUpdate();

        PreparedStatement deleteFlightDataStatement = connection.prepareStatement(deleteFlightDataQuery);
        deleteFlightDataStatement.setInt(1, plane.planeInfo.getId());
        deleteFlightDataStatement.executeUpdate();
    }
}
