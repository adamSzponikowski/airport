package controlTower;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import planes.Plane;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class CollisionDetectorTest {

    private CollisionDetector collisionDetector = new CollisionDetector();
    private ControlTowerDatabaseConnectionService controlTowerDBCS = new ControlTowerDatabaseConnectionService();
    private DatabaseConnection databaseConnection = DatabaseConnection.getInstance();
    private Connection connection = databaseConnection.getConnection();
    private IDGenerator idGenerator = new IDGenerator();


    @Test
    public void isCollisionBeingDetected() throws SQLException {
        //Given
        Plane plane = new Plane();
        idGenerator.generateUniqueRandomID(plane.planeInfo);
        plane.planeInfo.coordinates.setVertical(10);
        Plane plane1 = new Plane();
        idGenerator.generateUniqueRandomID(plane1.planeInfo);
        plane1.planeInfo.coordinates.setVertical(9);
        controlTowerDBCS.insertAPlaneIntoDatabase(plane.planeInfo);
        controlTowerDBCS.insertAPlaneIntoDatabase(plane1.planeInfo);
        //Then
        //Assertions.assertTrue(collisionDetector.didPlanesCrashed(plane.planeInfo));
    }

    @AfterEach
    public void removePlanesFromDB() throws SQLException {
        Statement statement = connection.createStatement();
        String sql = "DELETE FROM planes";
        String sql1 = "DELETE FROM flight_data";
        statement.execute(sql);
        statement.execute(sql1);
        statement.close();
    }
}
