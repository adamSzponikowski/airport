package controlTower;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import planes.Plane;
import planes.PlaneInfo;

public class ControlTowerJsonService {
    public PlaneInfo makeJsonStringAnPlaneObject(String clientMessage) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(clientMessage, PlaneInfo.class);
    }

    public String sendJsonToTheClient(PlaneInfo planeInfo) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(planeInfo);
    }
}
