package controlTower;

import planes.Plane;
import planes.PlaneInfo;

import java.sql.SQLException;
import java.util.Random;

public class IDGenerator {
    ControlTowerDatabaseConnectionService controlTowerDatabaseConnectionService = new ControlTowerDatabaseConnectionService();

    public void generateUniqueRandomID(PlaneInfo planeInfo) throws SQLException {
        int randomID;
        boolean uniqueIDFound = false;
        Random random = new Random();
        do {
            randomID = random.nextInt(10000);
            if (!controlTowerDatabaseConnectionService.doesIDExists(randomID)) {
                uniqueIDFound = true;
                planeInfo.setID(randomID);
            }
        } while (!uniqueIDFound);
    }
}
