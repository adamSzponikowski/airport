package controlTower;

import planes.Coordinates;
import planes.PlaneInfo;

import java.sql.SQLException;
import java.util.List;

public class CollisionDetector {

    public void didPlanesCrashed(PlaneInfo planeInfo, Coordinates coordinates, List<Coordinates> dbCoordinates) throws SQLException {
        for (Coordinates databaseCoordinates : dbCoordinates) {
            if (isCollision(coordinates, databaseCoordinates)) {
                planeInfo.setDid_crashed(true);
            }
        }
    }

    private boolean isCollision(Coordinates coordinates, Coordinates dbCoordinates) {
        if ((coordinates.getAltitude() - dbCoordinates.getAltitude() < 10)) {
            return true;
        }
        if ((coordinates.getVertical() - dbCoordinates.getVertical() < 10)) {
            return true;
        }
        if ((coordinates.getHorizontal() - dbCoordinates.getHorizontal() < 10)) {
            return true;
        }
        return false;
    }

    private boolean planesAreNear(Coordinates coordinates, Coordinates dbCoordinates) {
        if ((coordinates.getAltitude() - dbCoordinates.getAltitude() < 100)) {
            return true;
        }
        if ((coordinates.getVertical() - dbCoordinates.getVertical() < 100)) {
            return true;
        }
        if ((coordinates.getHorizontal() - dbCoordinates.getHorizontal() < 100)) {
            return true;
        }
        return false;
    }

    public void avoidACrash(PlaneInfo planeInfo, Coordinates coordinates, List<Coordinates> dbCoordinates) {
        for (Coordinates databaseCoordinates : dbCoordinates) {
            if (planesAreNear(planeInfo.coordinates, databaseCoordinates)) {
                if ((coordinates.getVertical() - databaseCoordinates.getVertical() < 100)) {
                    planeInfo.coordinates.setVertical(planeInfo.coordinates.getVertical() - 150);
                }
                if ((coordinates.getHorizontal() - databaseCoordinates.getHorizontal() < 100)) {
                    planeInfo.coordinates.setHorizontal(planeInfo.coordinates.getHorizontal() - 150);
                }
            }
        }
    }

}
