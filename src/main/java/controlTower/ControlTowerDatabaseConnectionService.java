package controlTower;

import planes.Coordinates;
import planes.Plane;
import planes.PlaneInfo;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ControlTowerDatabaseConnectionService {
    private DatabaseConnection databaseConnection = DatabaseConnection.getInstance();
    private Connection connection = databaseConnection.getConnection();
    private Timestamp createdAt = new Timestamp(System.currentTimeMillis());
    private Timestamp updatedAt = new Timestamp(System.currentTimeMillis());

    public boolean doesIDExists(int id) throws SQLException {
        String query = "SELECT COUNT(*) FROM planes WHERE plane_id = ?";
        try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                int count = resultSet.getInt(1);
                return count > 0;
            }
        }
        return false;
    }

    public void insertAPlaneIntoDatabase(PlaneInfo planeInfo) throws SQLException {
        String query = "INSERT INTO planes (plane_id,x,y,z, created_at, updated_at) VALUES (?,?,?,?,?,?)";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setInt(1, planeInfo.getId());
        preparedStatement.setInt(2, planeInfo.coordinates.getHorizontal());
        preparedStatement.setInt(3, planeInfo.coordinates.getVertical());
        preparedStatement.setInt(4, planeInfo.coordinates.getAltitude());
        preparedStatement.setTimestamp(5, createdAt);
        preparedStatement.setTimestamp(6, updatedAt);
        preparedStatement.executeUpdate();
    }

    public void updateAPlane(PlaneInfo planeInfo) throws SQLException {
        String query = "UPDATE planes SET x = ?, y = ?, z = ?, created_at = ?, landed = ?, crashed = ? WHERE plane_id = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setInt(1, planeInfo.coordinates.getHorizontal());
        preparedStatement.setInt(2, planeInfo.coordinates.getVertical());
        preparedStatement.setInt(3, planeInfo.coordinates.getAltitude());
        preparedStatement.setTimestamp(4, updatedAt);
        preparedStatement.setBoolean(5, planeInfo.isDid_landed());
        preparedStatement.setBoolean(6, planeInfo.isDid_crashed());
        preparedStatement.setInt(7, planeInfo.getId());

        preparedStatement.executeUpdate();
    }

    public List<Coordinates> getCoordinatesFromDatabase(PlaneInfo planeInfo) throws SQLException {
        List<Coordinates> coordinatesList = new ArrayList<>();
        String query = "SELECT plane_id,X,Y,Z from planes";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        ResultSet resultSet = preparedStatement.executeQuery();
        if (resultSet.next()) {
            int x = resultSet.getInt("X");
            int y = resultSet.getInt("Y");
            int z = resultSet.getInt("Z");
            int dbPlaneId = resultSet.getInt("plane_id");

            if (dbPlaneId != planeInfo.getId()) {
                Coordinates dbCoordinates = new Coordinates(x, y, z);
                coordinatesList.add(dbCoordinates);
            }
        }
        resultSet.close();
        preparedStatement.close();
        return coordinatesList;
    }
}
