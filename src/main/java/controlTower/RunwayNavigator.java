package controlTower;

import aiport.Runway;
import planes.Direction;
import planes.PlaneInfo;

import java.sql.SQLException;

public class RunwayNavigator {

    private Runway leftRunway = new Runway(1, true);
    private Runway rightRunway = new Runway(2, true);

    public void navigatePLaneToARunway(PlaneInfo planeInfo) throws SQLException {
        int currentHorizontal = planeInfo.coordinates.getHorizontal();
        chooseRunway(currentHorizontal, planeInfo);
        if (planeInfo.getRunway() != null) {
            movePlaneTowardsRunway(planeInfo, planeInfo.getRunway());
        } else {
            circleInPlace(planeInfo);
        }
    }

    private void chooseRunway(int currentHorizontal, PlaneInfo planeInfo) {
        int minRunway = 4000;
        int maxRunway = 6000;
        int runwayWidth = 1000;

        int runwayIndex = (currentHorizontal - minRunway) / runwayWidth;

        if (runwayIndex < 0) {
            runwayIndex = 0;
        } else if (runwayIndex >= (maxRunway - minRunway) / runwayWidth) {
            runwayIndex = (maxRunway - minRunway) / runwayWidth - 1;
        }

        int selectedRunway = runwayIndex + 1;

        if (selectedRunway == leftRunway.getRunwayNumber()) {
            if (leftRunway.isNotBusy()) {
                leftRunway.setNotBusy(false);
                planeInfo.setRunway(leftRunway);
            }
        }
        if (selectedRunway == rightRunway.getRunwayNumber()) {
            if (rightRunway.isNotBusy()) {
                rightRunway.setNotBusy(false);
                planeInfo.setRunway(rightRunway);
            }
        }
    }

    private void movePlaneTowardsRunway(PlaneInfo planeInfo, Runway selectedRunway) throws SQLException {
        int currentHorizontal = planeInfo.coordinates.getHorizontal();
        int runwayPosition = 4000 + selectedRunway.getRunwayNumber() * 1000;
        if (currentHorizontal < runwayPosition) {
            planeInfo.flightDirection.setHeading(Direction.RIGHT);
            planeInfo.coordinates.setHorizontal(planeInfo.coordinates.getHorizontal() + 50);
            if (currentHorizontal > 4000) {
                planeInfo.coordinates.setAltitude(0);
                planeInfo.setDid_landed(true);
                selectedRunway.setNotBusy(true);
            }
        }
        if (currentHorizontal > runwayPosition) {
            planeInfo.flightDirection.setHeading(Direction.LEFT);
            planeInfo.coordinates.setHorizontal(planeInfo.coordinates.getHorizontal() - 50);
            if (currentHorizontal < 6000) {
                planeInfo.coordinates.setAltitude(0);
                planeInfo.setDid_landed(true);
                selectedRunway.setNotBusy(true);
            }
        }
    }

    private void circleInPlace(PlaneInfo planeInfo) {
        Direction currentHeading = planeInfo.flightDirection.getHeading();
        int currentHorizontal = planeInfo.coordinates.getHorizontal();
        int leftRunwayPosition = 4000;
        int rightRunwayPosition = 5000;

        int circleDistance = 50;

        if (currentHeading == Direction.RIGHT) {

            int newHorizontal = currentHorizontal - circleDistance;
            if (newHorizontal < leftRunwayPosition) {
                planeInfo.flightDirection.setHeading(Direction.LEFT);
            } else {
                planeInfo.coordinates.setHorizontal(newHorizontal);
            }
        } else if (currentHeading == Direction.LEFT) {
            int newHorizontal = currentHorizontal + circleDistance;
            if (newHorizontal > rightRunwayPosition) {
                planeInfo.flightDirection.setHeading(Direction.RIGHT);
            } else {
                planeInfo.coordinates.setHorizontal(newHorizontal);
            }
        } else {
            planeInfo.flightDirection.setHeading(Direction.RIGHT);
        }
    }
}
