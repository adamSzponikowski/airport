package controlTower;

import aiport.Runway;
import planes.Coordinates;
import planes.Plane;
import planes.PlaneClientService;
import planes.PlaneInfo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.SQLException;
import java.util.List;

public class ControlTower {

    private ControlTowerJsonService controlTowerJsonService = new ControlTowerJsonService();
    private ControlTowerDatabaseConnectionService controlTowerDBService = new ControlTowerDatabaseConnectionService();
    private RunwayNavigator runwayNavigator = new RunwayNavigator();
    private IDGenerator idGenerator = new IDGenerator();
    private ServerSocket serverSocket;
    private Socket clientSocket;
    private CollisionDetector collisionDetector = new CollisionDetector();

    public void startServer() throws IOException {
        serverSocket = new ServerSocket(PlaneClientService.PORT_NUMBER);
        clientSocket = serverSocket.accept();
    }

    public static void main(String[] args) throws IOException, SQLException {
        ControlTower controlTower = new ControlTower();
        controlTower.startServer();
        controlTower.communicate();
    }

    public void communicate() throws IOException, SQLException {
        BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);


        String clientMessage;
        boolean idAssigned = false;
        boolean isInsertedIntoDatabase = false;
        while ((clientMessage = in.readLine()) != null) {
            PlaneInfo planeInfo = controlTowerJsonService.makeJsonStringAnPlaneObject(clientMessage);
            List<Coordinates> databaseCoordinates = controlTowerDBService.getCoordinatesFromDatabase(planeInfo);

            if (!idAssigned) {
                idGenerator.generateUniqueRandomID(planeInfo);
                idAssigned = true;
            }
            if (!isInsertedIntoDatabase) {
                controlTowerDBService.insertAPlaneIntoDatabase(planeInfo);
                isInsertedIntoDatabase = true;
            }
            collisionDetector.didPlanesCrashed(planeInfo,planeInfo.coordinates,databaseCoordinates);
            collisionDetector.avoidACrash(planeInfo,planeInfo.coordinates,databaseCoordinates);
            runwayNavigator.navigatePLaneToARunway(planeInfo);
            controlTowerDBService.updateAPlane(planeInfo);
            out.println(controlTowerJsonService.sendJsonToTheClient(planeInfo));
        }
    }
}


