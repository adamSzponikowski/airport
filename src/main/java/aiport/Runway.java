package aiport;

public class Runway {
    private int runwayNumber;
    private boolean isNotBusy;

    public Runway(int index, boolean isBusy) {
        this.runwayNumber = index;
        this.isNotBusy = isBusy;
    }

    public Runway() {
    }

    public int getRunwayNumber() {
        return runwayNumber;
    }
    public void setRunwayNumber(int runwayNumber) {
        this.runwayNumber = runwayNumber;
    }
    public boolean isNotBusy() {
        return isNotBusy;
    }
    public void setNotBusy(boolean notBusy) {
        isNotBusy = notBusy;
    }
}
