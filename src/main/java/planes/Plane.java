package planes;

import java.net.Socket;
import java.time.LocalDateTime;

public class Plane {

    public PlaneInfo planeInfo;
    private Socket socket;

    public Plane() {
        this.planeInfo = new PlaneInfo();
    }

    public PlaneInfo getPlaneInfo() {
        return planeInfo;
    }

    public void setPlaneInfo(PlaneInfo planeInfo) {
        this.planeInfo = planeInfo;
    }

    public void setSocket(Socket socket) {
        this.socket = socket;
    }

    public void updatePlaneObject(Plane plane, PlaneInfo updatedPlaneInfo) {
        plane.planeInfo.setID(updatedPlaneInfo.getId());
        plane.getPlaneInfo().setCoordinates(updatedPlaneInfo.getCoordinates());
        plane.planeInfo.setDid_landed(updatedPlaneInfo.isDid_landed());
        plane.planeInfo.setRunway(updatedPlaneInfo.getRunway());
    }

}
