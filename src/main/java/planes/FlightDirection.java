package planes;

import java.util.Random;

public class FlightDirection {
    private Direction heading;
    public FlightDirection() {
        generateRandomDirection();
    }
    private void generateRandomDirection() {
        Random random = new Random();
        int directionIndex = random.nextInt(Direction.values().length);
        heading = Direction.values()[directionIndex];
    }

    public Direction getHeading() {
        return heading;
    }

    public void setHeading(Direction heading) {
        this.heading = heading;
    }
}
