package planes;

import java.util.Random;

public class Coordinates {
    private int horizontal;
    private int vertical;
    private int altitude;

    public Coordinates() {
        generateRandomCoordinates();
    }

    public Coordinates(int x, int y, int z) {
        this.horizontal = x;
        this.vertical = y;
        this.altitude = z;
    }

    private void generateRandomCoordinates() {
        Random random = new Random();

        int side = random.nextInt(4);
        altitude = random.nextInt(3000) + 2000;


        switch (side) {
            case 0:
                horizontal = random.nextInt(10001);
                vertical = 0;
                break;
            case 1:
                horizontal = 10000;
                vertical = random.nextInt(10001);
                break;
            case 2:
                horizontal = random.nextInt(10001);
                vertical = 10000;
                break;
            case 3:
                horizontal = 0;
                vertical = random.nextInt(10001);
                break;
        }

        while (horizontal >= 4000 && horizontal <= 6000) {
            horizontal = random.nextInt(10001);
        }
    }

    public int getAltitude() {
        return altitude;
    }

    public int getHorizontal() {
        return horizontal;
    }

    public int getVertical() {
        return vertical;
    }

    public void setHorizontal(int horizontal) {
        this.horizontal = horizontal;
    }

    public void setVertical(int vertical) {
        this.vertical = vertical;
    }

    public void setAltitude(int altitude) {
        this.altitude = altitude;
    }
}
