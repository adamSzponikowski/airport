package planes;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public class PlaneJsonService {
    public String sendJsonToTheServer(Plane plane) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
        return objectMapper.writeValueAsString(plane.planeInfo);
    }

    public PlaneInfo makeJsonStringAnPlaneObject(String controlTowerMessage) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(controlTowerMessage, PlaneInfo.class);
    }

}
