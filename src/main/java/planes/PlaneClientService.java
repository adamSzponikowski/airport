package planes;


import connectionsController.ConnectionController;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;


public class PlaneClientService {
    private PlaneJsonService planeJsonService = new PlaneJsonService();
    private ConnectionController connectionController = new ConnectionController();
    private String ip = "localhost";
    public static final int PORT_NUMBER = 7686;
    private Socket clientSocket;
    private PrintWriter out;
    private BufferedReader in;


    public void startConnection(Plane plane) throws IOException {
        clientSocket = new Socket(ip, PORT_NUMBER);
        plane.setSocket(clientSocket);
        communicate(plane);
    }

    public void communicate(Plane plane) throws IOException {
        while (!plane.planeInfo.isDid_landed()) {
            out = new PrintWriter(clientSocket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            String output = planeJsonService.sendJsonToTheServer(plane);
            connectionController.addAPlaneToList(plane.planeInfo);
            out.println(output);
            String serverMessage = in.readLine();
            PlaneInfo updatedPlaneInfo = planeJsonService.makeJsonStringAnPlaneObject(serverMessage);
            plane.updatePlaneObject(plane, updatedPlaneInfo);
            connectionController.removeAPlane(updatedPlaneInfo);
            PlaneLoggerService.logJson(serverMessage);
        }
    }
}
