package planes;

import org.apache.log4j.LogManager;

import java.util.logging.Logger;

public class PlaneLoggerService {
    private static final Logger logger = Logger.getLogger(PlaneLoggerService.class.getName());
    public static void logJson(String json) {
        logger.info(json);
    }
}

