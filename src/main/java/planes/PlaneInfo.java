package planes;

import aiport.Runway;

public class PlaneInfo {
    public PlaneInfo() {
        this.coordinates = new Coordinates();
        this.flightDirection = new FlightDirection();
    }

    private Runway runway;
    public FlightDirection flightDirection;
    private int ID;
    public Coordinates coordinates;
    private boolean did_crashed = false;
    private boolean did_landed = false;

    public void setDid_crashed(boolean did_crashed) {
        this.did_crashed = did_crashed;
    }

    public void setDid_landed(boolean did_landed) {
        this.did_landed = did_landed;
    }

    public boolean isDid_crashed() {
        return did_crashed;
    }

    public boolean isDid_landed() {
        return did_landed;
    }

    public Coordinates getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(Coordinates coordinates) {
        this.coordinates = coordinates;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getId() {
        return ID;
    }

    public void setRunway(Runway runway) {
        this.runway = runway;
    }

    public Runway getRunway() {
        return runway;
    }
}
