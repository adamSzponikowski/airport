package planes;

import java.io.IOException;

public class PlaneService {
    public static void main(String[] args) throws IOException {
        PlaneClientService planeClientService = new PlaneClientService();
        Plane plane = new Plane();
        planeClientService.startConnection(plane);
    }
}
