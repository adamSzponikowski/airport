package planes;

public enum Direction {
    UP,
    DOWN,
    LEFT,
    RIGHT
}
