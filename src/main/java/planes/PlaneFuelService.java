package planes;

import java.sql.Timestamp;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.temporal.Temporal;

public class PlaneFuelService {
    public PlaneFuelService() {
        this.createdAt = new Timestamp(System.currentTimeMillis());
    }

    private Timestamp createdAt;

    public boolean hasFuelExpired() {
        LocalDateTime currentTime = LocalDateTime.now();
        Duration timeSinceCreation = Duration.between((Temporal) createdAt, currentTime);
        long hoursSinceCreation = timeSinceCreation.toHours();
        return hoursSinceCreation >= 3;
    }
}
