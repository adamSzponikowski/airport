package connectionsController;

import planes.Plane;
import planes.PlaneInfo;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

public class ConnectionController {
    private final int MAXIMUM_CONNECTIONS = 100;
    private volatile List<PlaneInfo> planes;
    private ReentrantLock lock = new ReentrantLock();

    private boolean isThereAPlace() {
        return planes.size() <= MAXIMUM_CONNECTIONS;
    }

    public void removeAPlane(PlaneInfo planeInfo) {
        lock.lock();
        try {
            Iterator<PlaneInfo> iterator = planes.iterator();
            while (iterator.hasNext()) {
                PlaneInfo plane = iterator.next();
                if (plane.getId() == planeInfo.getId() && plane.isDid_landed() || plane.isDid_crashed()) {
                    iterator.remove();
                    break;
                }
            }
        } finally {
            lock.unlock();
        }
    }

    public void addAPlaneToList(PlaneInfo planeInfo) {
        lock.lock();
        try {
            if (isThereAPlace()) {
                planes.add(planeInfo);
            } else {
                System.out.println("You can not land on this airport!");
            }
        } finally {
            lock.unlock();
        }
    }
}
